import java.util.Scanner;
import java.util.ArrayList;

class Fahrkartenautomat
// neues commit, da ich ich falsche Aufgabe eingebeben habe
{

	public static void main(String[] args) {
		// Fahrkartenkosten und -anzahl
		// -----------
		double zuZahlenderBetrag = fahrkartenbestellungErfassen();

		// Geldeinwurf
		// -----------
		double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

		// Fahrscheinausgabe
		// -----------------
		fahrkartenAusgeben();

		// R�ckgeldberechnung und -Ausgabe
		// -------------------------------
		rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
	}

	public static double fahrkartenbestellungErfassen() {
		boolean at = false;
		double summe = 0;
		Kasse: do {
			Scanner tastatur = new Scanner(System.in);
			System.out.println("W�hlen sie ihre Wunschfahrkarte f�r Berlin AB aus: ");
			System.out.println("  Einzelfahrschein Regeltarif AB [2,90�] (1) ");
			System.out.println("  Tageskarte Regeltarif [8,60]� (2) ");
			System.out.println("  Kleingruppen-Tageskarte Regeltarif AB [23.50�] (3)");
			System.out.println("  Bezahlen (9)");

			double zuZahlenderBetrag = -1;
			boolean bt = true;
			do {
				bt = true;
				int ihreWahl = tastatur.nextInt();
				System.out.println("Ihre Wahl: " + ihreWahl);

				switch (ihreWahl) {
				case 1:
					zuZahlenderBetrag = 2.90;
					break;
				case 2:
					zuZahlenderBetrag = 8.60;
					break;
				case 3:
					zuZahlenderBetrag = 23.50;
					break;
				case 9:
					at = true;
					break Kasse;
				default:
					System.out.println(" >>FALSCHE EINGABE<< ");
					bt = false;
					break;
				}
			} while (bt == false);

			int anzahlTickets = -1;

			while (anzahlTickets < 1 || anzahlTickets > 10) {
				System.out.println("Wieviele Tickets m�chten sie kaufen? ");
				// Ich habe �ber google die "try-catch"-phrase gefunden und wollte sie testen, da sie mein Problem l�ste.
				try {
					anzahlTickets = tastatur.nextInt();
					if (anzahlTickets < 1 || anzahlTickets > 10) {
						System.out.println("Falsche Eingabe, bitte geben sie eine ganze Zahl zwischen 1 und 10 ein.");
					}
				} catch (Exception e) {
					tastatur.nextLine();
					System.out.println(" Falsche Eingabe, bitte geben sie eine ganze Zahl von 1 bis 10 ein.");
					continue;
				}
			}
			double zuZahlendeZwischensumme = zuZahlenderBetrag * anzahlTickets;
			summe += zuZahlendeZwischensumme;
			System.out.printf("Zwischensumme: %.2f Euro \n", summe );

		} while (at == false);
		return summe;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f Euro", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
			double eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;

		if (r�ckgabebetrag > 0.0) {
			r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro ", r�ckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				r�ckgabebetrag -= 2.0;
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				r�ckgabebetrag -= 1.0;
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				r�ckgabebetrag -= 0.5;
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				r�ckgabebetrag -= 0.2;
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				r�ckgabebetrag -= 0.1;
			}
			while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				r�ckgabebetrag -= 0.05;
			}
		}
	}
}
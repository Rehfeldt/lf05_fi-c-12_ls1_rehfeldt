
public class Auto {

	private String marke;
	private String farbe;
	
	private String bestizer;
	private int bauJahr;
	private int hubraum;
	

	public Auto() {
		System.out.println("Auto-Objekt");
	}

	public Auto(String marke, String farbe) {

		System.out.println("Auto-Objekt mit Parameter");

		this.marke = marke;
		this.farbe = farbe;
	}
// getter setter
	public void setMarke(String marke) {
		this.marke = marke;
	}

	public String getMarke() {
		return this.marke;

	}
	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}

	public String getFarbe() {
		return this.farbe;
		
	}
	public String getBestizer() {
		return bestizer;
	}

	public void setBestizer(String bestizer) {
		this.bestizer = bestizer;
	}

	public int getBauJahr() {
		return bauJahr;
	}

	public void setBauJahr(int bauJahr) {
		this.bauJahr = bauJahr;
	}

	public int getHubraum() {
		return hubraum;
	}

	public void setHubraum(int hubraum) {
		this.hubraum = hubraum;
	}
// ende getter setter
	public void fahre(int strecke) {
		
		System.out.println("Los gehts!!!");
	}
	public void tanke(int liter) {
		
		System.out.println("Ready to ride!!");
	}
}
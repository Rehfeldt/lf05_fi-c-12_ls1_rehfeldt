import java.util.Scanner;

public class PCHaendler {
	public static String liesString() 
	{
		Scanner myScanner = new Scanner(System.in);
		String x = myScanner.next();
		return x;
	}
	
	public static int liesint()
	{
		Scanner myScanner = new Scanner(System.in);
		int y = myScanner.nextInt();
		return y;
	}
	public static double liesDouble()
	{
		Scanner myScanner = new Scanner(System.in);
		double z = myScanner.nextDouble();
		return z;
	}
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis)
	{
		double a = anzahl * nettopreis;
		return a ;
	}
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst)
	{
		double b = mwst * nettogesamtpreis / 100 + nettogesamtpreis;
		return b ;
	}
	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst)
	{
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
	public static void main(String[] args) 
	{
		// Benutzereingaben lesen
		System.out.println("was m�chten Sie bestellen?");
		String artikel=liesString();
		
		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl=liesint();
		
		System.out.println("Geben Sie den Nettopreis ein:");
		double preis=liesDouble();
		
		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		//double mwst = myScanner.nextDouble();
		double mwst = liesDouble();

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben

		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

	}

}



public class Ausf�hren {

	public static void main(String[] args) {

		Ladung ldg1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung ldg2 = new Ladung("Borg-Schrott", 5);
		Ladung ldg3 = new Ladung("Rote Materie", 2);
		Ladung ldg4 = new Ladung("Forschungssonde", 35);
		Ladung ldg5 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung ldg6 = new Ladung("Plasma-Waffe", 50);
		Ladung ldg7 = new Ladung("Photonentorpedo", 3);

		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");

		klingonen.addLadung(ldg1);
		klingonen.addLadung(ldg5);

		romulaner.addLadung(ldg2);
		romulaner.addLadung(ldg3);
		romulaner.addLadung(ldg6);

		vulkanier.addLadung(ldg4);
		vulkanier.addLadung(ldg7);
		//Zusand ausgeben
		System.out.println("Klingonenraumschiff:");
		klingonen.zustandRaumschiff();
		System.out.println("Vulkanierraumschiff:");
		vulkanier.zustandRaumschiff();
		System.out.println("Romulanerraumschiff:");
		romulaner.zustandRaumschiff();
		//Ladungsverzeichnis ausgeben
		System.out.println("Klingonenschiff:");
		klingonen.ladungsverzeichnisAusgeben();
		System.out.println("Romulanerschiff:");
		romulaner.ladungsverzeichnisAusgeben();
		System.out.println("Vulkanierschiff:");
		vulkanier.ladungsverzeichnisAusgeben();
		//Photonentorpedos schie�en
		klingonen.photonentorpedoSchiessen(romulaner.getSchiffsname());
		romulaner.photonentorpedoSchiessen(vulkanier.getSchiffsname());
		vulkanier.photonentorpedoSchiessen(klingonen.getSchiffsname());
		//Phaserkanonen schie�en
		klingonen.phaserkanoneSchiessen(romulaner.getSchiffsname());
		romulaner.phaserkanoneSchiessen(vulkanier.getSchiffsname());
		vulkanier.phaserkanoneSchiessen(klingonen.getSchiffsname());

		
	}

}

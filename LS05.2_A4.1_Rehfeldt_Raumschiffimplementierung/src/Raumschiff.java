import java.util.ArrayList;

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int LebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	public Raumschiff() {
		System.out.println("Raumschiffobjekt");
	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int LebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {

		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.LebenserhaltungssystemeInProzent = LebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;

	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getenergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setenergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int gethuelleInProzent() {
		return huelleInProzent;
	}

	public void sethuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return LebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		LebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

// Ende Getter Setter

	public void addLadung(Ladung ladung) {

		this.ladungsverzeichnis.add(ladung);

	}

	public void phaserkanoneSchiessen(String ziel) {
		if (energieversorgungInProzent < 50) {

			nachrichtAnAlle(schiffsname + " -=*Click*=-");
		} 
		else {
			energieversorgungInProzent = energieversorgungInProzent - 50;
			nachrichtAnAlle("Die " + schiffsname + " hat seine Phaser abgefeuert!");
			treffer(ziel);

		}
	}

	public void photonentorpedoSchiessen(String ziel) {

		if (photonentorpedoAnzahl == 0) {
			nachrichtAnAlle(schiffsname + " -=*Click*=-");
		} 
		else {
			photonentorpedoAnzahl = photonentorpedoAnzahl - 1;
			nachrichtAnAlle("Die " + schiffsname + " hat einen Photonentorpedo abgeschossen!");
			treffer(ziel);
			
		}

	}

	private void treffer(String gegner) {

		System.out.println("Die " + gegner + " wurde getroffen!");
		System.out.println(" ");
	}

	public void nachrichtAnAlle(String message) {

		System.out.println(message);
		System.out.println(" ");

	}

	public void photonentorpedosLaden(int AnzahlTorpedos) {

	}

	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int anzahlDroiden) {

	}

	public void zustandRaumschiff() {

		System.out.println("Schiffsname: " + this.schiffsname);
		System.out.println("Photonentorpedoanzahl: " + this.photonentorpedoAnzahl);
		System.out.println("energieversorgungInProzent: " + this.energieversorgungInProzent);
		System.out.println("SchildeInProzent: " + this.schildeInProzent);
		System.out.println("huelleInProzent: " + this.huelleInProzent);
		System.out.println("LebenserhaltungssystemeInProzent: " + this.LebenserhaltungssystemeInProzent);
		System.out.println("AndroidenAnzahl: " + this.androidenAnzahl);
		System.out.println(" ");

	}

	public void ladungsverzeichnisAusgeben() {

		System.out.println(ladungsverzeichnis.toString());
		System.out.println(" ");

	}

	public void ladungsverzeichnisAufraeumen() {

	}
}

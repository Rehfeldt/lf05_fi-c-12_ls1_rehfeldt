
/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author
    @version
*/
public class Variablen {
  public static void main(String [] args){
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */
	  
	  /* In 2. Deklariert und initialisiert

    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/

	  int zaehler = 25;
	  System.out.println("Programmdurchl�ufe: " + zaehler);
	  
    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */

    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/

	  char menue = 'C';
	  System.out.println("W�hle Men�punkt: " + menue);
	  
	  
    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */

    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
	  
	  final int LICHTGESCHWINDIGKEIT = 300000;
	  System.out.println("Lichtgeschwindigkeit: " + LICHTGESCHWINDIGKEIT);

    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
	  
	  int vereinsMitglieder = 7;
	  
	  
    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
	  
	  System.out.println("Mitglieder des Vereins: " + vereinsMitglieder);

    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
	  
	  final double ELEMENTARLADUNG = 1.6E-19;
	  System.out.println("Elektrische Elementarladung : " + ELEMENTARLADUNG);
	  

    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
	  
	  boolean zahlung = true;
	 

    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
	  
	  System.out.println("Zahlung erfolgt? " + zahlung);

  }//main
}// Variablen